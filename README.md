# Legion 5 pro Keyboard Backlight UI

UI for https://github.com/imShara/l5p-kbl

widget

## Dependences

sudo apt-get install qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools
sudo apt install qttools5-dev-tools
sudo apt install pyqt5-dev-tools


### Applet
use wheel to switch preset
wheel click to turn on/off backlight


## TODO
- [x] create ModernToggle
- [x] create ModernRadioButton
- [x] paramitraze Modern elements
- [x] create main app with modern elements
- [x] use buttons instread of Keyboard widget
- [x] use default colour picker
- [x] use tray icon (hide main window)
- [x] add labels
- [x] remove hue button
- [x] add speed slider
- [x] add wave direction
- [x] stubs for actions (main window)
- [x] stubs for actions (tray)
- [x] add buttons logic
- [x] **CONNECT TO l5p_kbl** (set current state)
- [x] on/off btn
- [] presets
- [] config styles, layouts, labels etc
- [] refactoring
- [] add auto apply
- [] get current configuration from keyboard
- [] add logger
- [] create Keyboard widget
- [] create custom modern colour picker
