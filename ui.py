from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSlot

from qt_custom import ModernToggle, ModernRadioButton, ModernSlider

from l5p_kbl import LedController


class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.resize(1200, 800)
        self.setWindowIcon(QIcon('icon.png'))
        self.setWindowTitle('Lenovo Legion 5 Pro keyboard backlight manager')

        # main container
        self.container = QFrame()
        self.container.setObjectName('main-continer')
        self.container.setStyleSheet('#main-continer { background-color: #262626 }')
        self.layout = QVBoxLayout()
        self.container.setLayout(self.layout)
        self.setCentralWidget(self.container)

        # container -> container_top
        self.container_top = QFrame(self.container)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.container_top.sizePolicy().hasHeightForWidth())
        self.container_top.setSizePolicy(sizePolicy)
        self.container_top.setFrameShape(QFrame.StyledPanel)
        self.container_top.setFrameShadow(QFrame.Raised)
        self.container_top.setObjectName("container_top")
        self.container_top.layout = QVBoxLayout()
        self.container_top.setLayout(self.container_top.layout)

        # container -> container_top -> hl_top
        self.hl_top = QHBoxLayout()
        self.hl_top.setObjectName("hl_top")

        # container -> container_top -> hl_top -> gb_mode
        self.gb_mode = QGroupBox(self.container_top)
        self.gb_mode.setObjectName("gb_mode")
        self.gb_mode.layout = QHBoxLayout(self.gb_mode)
        self.rb_mode_static = ModernRadioButton('Static')
        self.rb_mode_breath = ModernRadioButton('Breath')
        self.rb_mode_wave = ModernRadioButton('Wave')
        self.rb_mode_static.setChecked(True)
        self.rb_mode_static.toggled.connect(self.mode_click)
        self.rb_mode_breath.toggled.connect(self.mode_click)
        self.rb_mode_wave.toggled.connect(self.mode_click)
        # self.rb_mode_hue = ModernRadioButton('Hue')
        self.gb_mode.layout.addWidget(self.rb_mode_static)
        self.gb_mode.layout.addWidget(self.rb_mode_breath)
        self.gb_mode.layout.addWidget(self.rb_mode_wave)
        # self.gb_mode.layout.addWidget(self.rb_mode_hue)
        self.gb_mode.setLayout(self.gb_mode.layout)
        
        # container -> container_top -> hl_top -> spacer_hl_top
        self.spacer_hl_top = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        
        # container -> container_top -> hl_top -> gb_preset
        self.gb_preset = QGroupBox(self.container_top)
        self.gb_preset.setObjectName("gb_preset")
        self.gb_preset.layout = QHBoxLayout(self.gb_preset)
        self.rb_preset_1 = ModernRadioButton('Preset 1')
        self.rb_preset_2 = ModernRadioButton('Preset 2')
        self.rb_preset_3 = ModernRadioButton('Preset 3')
        self.rb_preset_1.setChecked(True)
        self.rb_preset_1.toggled.connect(self.preset_click)
        self.rb_preset_2.toggled.connect(self.preset_click)
        self.rb_preset_3.toggled.connect(self.preset_click)
        self.gb_preset.layout.addWidget(self.rb_preset_1)
        self.gb_preset.layout.addWidget(self.rb_preset_2)
        self.gb_preset.layout.addWidget(self.rb_preset_3)
        self.gb_preset.setLayout(self.gb_preset.layout)

        self.hl_top.addWidget(self.gb_mode)
        self.hl_top.addItem(self.spacer_hl_top)
        self.hl_top.addWidget(self.gb_preset)

        self.container_top.layout.addLayout(self.hl_top)


        # container -> container_top -> hl_bottom
        self.hl_bottom = QHBoxLayout()
        self.hl_bottom.setObjectName("hl_bottom")

        # container -> container_top -> hl_bottom -> toggles
        self.tg_one_color = ModernToggle()
        self.tg_br_level = ModernToggle()
        self.tg_on_off = ModernToggle()
        # container -> container_top -> hl_bottom -> spacer_hl_bottom
        self.spacer_hl_bottom = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.lb_one_color = QLabel(self.container_top, text='One color')
        self.spacer_hl_bottom1 = QSpacerItem(20, 20, QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.lb_br_level = QLabel(self.container_top, text='Brightness level')
        self.lb_on_off = QLabel(self.container_top, text='On/Off')
        self.sl_speed = ModernSlider()
        self.sl_speed.setOrientation(Qt.Horizontal)
        self.sl_speed.setMinimumHeight(150)
        self.sl_speed.setMaximum(3)
        self.sl_speed.setPageStep(1)
        self.sl_speed.valueChanged.connect(self.sl_speed_changed)
        self.sl_speed.setEnabled(False)
        self.lb_speed = QLabel(self.container_top, text='Speed')
        self.tg_wave_direction = ModernToggle()
        self.lb_wave_direction = QLabel(self.container_top, text='Wave to left <<<')

        self.tg_one_color.clicked.connect(self.tg_one_color_click)
        self.tg_on_off.setChecked(True)
        self.tg_br_level.clicked.connect(self.tg_br_level_click)
        self.tg_on_off.clicked.connect(self.tg_on_off_click)
        self.tg_wave_direction.clicked.connect(self.tg_wave_direction_click)
        self.lb_one_color.setFont(QFont('Arial', 14))
        self.lb_one_color.setStyleSheet(f"color: #006996")
        self.lb_br_level.setFont(QFont('Arial', 14))
        self.lb_br_level.setStyleSheet(f"color: #00bcff")
        self.lb_on_off.setFont(QFont('Arial', 14))
        self.lb_on_off.setStyleSheet(f"color: #006996")
        self.lb_wave_direction.setFont(QFont('Arial', 14))
        self.lb_wave_direction.setStyleSheet(f"color: #00bcff")
        self.lb_speed.setFont(QFont('Arial', 14))
        self.lb_speed.setStyleSheet(f"color: #00bcff")

        self.hl_bottom.addWidget(self.tg_one_color)
        self.hl_bottom.addWidget(self.lb_one_color)
        self.hl_bottom.addItem(self.spacer_hl_bottom1)
        self.hl_bottom.addWidget(self.tg_br_level)
        self.hl_bottom.addWidget(self.lb_br_level)
        self.hl_bottom.addWidget(self.sl_speed)
        self.hl_bottom.addWidget(self.lb_speed)
        self.hl_bottom.addWidget(self.tg_wave_direction)
        self.hl_bottom.addWidget(self.lb_wave_direction)
        self.hl_bottom.addItem(self.spacer_hl_bottom)
        self.hl_bottom.addWidget(self.lb_on_off)
        self.hl_bottom.addWidget(self.tg_on_off)

        self.container_top.layout.addLayout(self.hl_bottom)
        

        # container_bottom
        self.container_bottom = QFrame(self.container)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.container_bottom.sizePolicy().hasHeightForWidth())
        self.container_bottom.setSizePolicy(sizePolicy)
        self.container_bottom.setFrameShape(QFrame.StyledPanel)
        self.container_bottom.setFrameShadow(QFrame.Raised)
        self.container_bottom.setObjectName("container_bottom")
        self.container_bottom.layout = QVBoxLayout()
        self.container_bottom.setLayout(self.container_bottom.layout)

        # container -> container_bottom -> hl_bottom2
        self.hl_bottom2 = QHBoxLayout()
        self.hl_bottom2.setObjectName("hl_bottom2")

        # container -> container_bottom -> hl_bottom2 -> buttons
        # TODO: use list
        self.btn1 = QPushButton(self.container_bottom, text='section 1')
        self.btn2 = QPushButton(self.container_bottom, text='section 2')
        self.btn3 = QPushButton(self.container_bottom, text='section 3')
        self.btn4 = QPushButton(self.container_bottom, text='section 4')
        self.btn1.setObjectName('btn1')
        self.btn2.setObjectName('btn2')
        self.btn3.setObjectName('btn3')
        self.btn4.setObjectName('btn4')

        self.btn1.clicked.connect(self.btn1_click)
        self.btn2.clicked.connect(self.btn2_click)
        self.btn3.clicked.connect(self.btn3_click)
        self.btn4.clicked.connect(self.btn4_click)

        self.hl_bottom2.addWidget(self.btn1)
        self.hl_bottom2.addWidget(self.btn2)
        self.hl_bottom2.addWidget(self.btn3)
        self.hl_bottom2.addWidget(self.btn4)

        self.container_bottom.layout.addLayout(self.hl_bottom2)

        self.bt_submit = QPushButton(self.container_bottom, text='Submit')
        self.bt_submit.setStyleSheet("background-color: #1df420")
        self.bt_submit.clicked.connect(self.bt_submit_click)

        self.layout.addWidget(self.container_top)
        self.layout.addWidget(self.container_bottom)
        self.layout.addWidget(self.bt_submit)
        
        # tray icon
        self.trayIcon = QSystemTrayIcon(QIcon('icon.png'), parent=self)
        self.trayIcon.setToolTip('Manage keyboard backlight')
        self.trayIcon.show()
        self.trayIcon.activated.connect(self.tray_activated)

        self.tray_menu = QMenu()
        self.exitAction = self.tray_menu.addAction('Exit')
        self.exitAction.triggered.connect(app.quit)
        on_off_action = self.tray_menu.addAction('On/Off')
        on_off_action.triggered.connect(self.tg_on_off.click)
        preset1_action = self.tray_menu.addAction('Preset #1')
        preset1_action.triggered.connect(self.rb_preset_1.click)
        preset2_action = self.tray_menu.addAction('Preset #2')
        preset2_action.triggered.connect(self.rb_preset_2.click)
        preset3_action = self.tray_menu.addAction('Preset #3')
        preset3_action.triggered.connect(self.rb_preset_3.click)

        self.trayIcon.setContextMenu(self.tray_menu)
        self.show()

        self.effect = self.rb_mode_static.text().lower()
        self.colors = ['ff0000', '00ff00', '0000ff', 'ffffff']
        self.speed = self.sl_speed.value() + 1
        self.brightness = int(self.tg_br_level.isChecked()) + 1
        self.wave_direction = 'ltr' if self.tg_wave_direction else 'rtl'

        self.led_controller = LedController()

    def tray_activated(self, reason):
        if reason == QSystemTrayIcon.MiddleClick:
            # todo toggle backligth
            print('Middle')
        #  if reason == QSystemTrayIcon.DoubleClick:
        if reason == QSystemTrayIcon.Trigger:
            if self.isHidden():
                self.show()
            else:
                self.hide()
                
        
    @pyqtSlot()
    def btn1_click(self):
        color = self.openColorDialog()
        if self.tg_one_color.isChecked():
            self.colors = [color.name().replace('#', '')]*4
            self.btn1.setStyleSheet(f"background-color: {color.name()}")
            self.btn2.setStyleSheet(f"background-color: {color.name()}")
            self.btn3.setStyleSheet(f"background-color: {color.name()}")
            self.btn4.setStyleSheet(f"background-color: {color.name()}")
        else:
            self.btn1.setStyleSheet(f"background-color: {color.name()}")
            self.colors[0] = color.name().replace('#', '')

    @pyqtSlot()
    def btn2_click(self):
        color = self.openColorDialog()
        if self.tg_one_color.isChecked():
            self.colors = [color.name().replace('#', '')]*4
            self.btn1.setStyleSheet(f"background-color: {color.name()}")
            self.btn2.setStyleSheet(f"background-color: {color.name()}")
            self.btn3.setStyleSheet(f"background-color: {color.name()}")
            self.btn4.setStyleSheet(f"background-color: {color.name()}")
        else:
            self.btn2.setStyleSheet(f"background-color: {color.name()}")
            self.colors[1] = color.name().replace('#', '')

    @pyqtSlot()
    def btn3_click(self):
        color = self.openColorDialog()
        if self.tg_one_color.isChecked():
            self.colors = [color.name().replace('#', '')]*4
            self.btn1.setStyleSheet(f"background-color: {color.name()}")
            self.btn2.setStyleSheet(f"background-color: {color.name()}")
            self.btn3.setStyleSheet(f"background-color: {color.name()}")
            self.btn4.setStyleSheet(f"background-color: {color.name()}")
        else:
            self.btn3.setStyleSheet(f"background-color: {color.name()}")
            self.colors[2] = color.name().replace('#', '')

    @pyqtSlot()
    def btn4_click(self):
        color = self.openColorDialog()
        if self.tg_one_color.isChecked():
            self.colors = [color.name().replace('#', '')]*4
            self.btn1.setStyleSheet(f"background-color: {color.name()}")
            self.btn2.setStyleSheet(f"background-color: {color.name()}")
            self.btn3.setStyleSheet(f"background-color: {color.name()}")
            self.btn4.setStyleSheet(f"background-color: {color.name()}")
        else:
            self.btn4.setStyleSheet(f"background-color: {color.name()}")
            self.colors[3] = color.name().replace('#', '')

    @pyqtSlot()
    def tg_one_color_click(self):
        if self.tg_one_color.isChecked():
            print('Turn On one color')
            self.lb_one_color.setStyleSheet(f"color: #00bcff")
            color = self.btn1.palette().window().color()
            self.colors = [color.name().replace('#', '')]*4
            self.btn1.setStyleSheet(f"background-color: {color.name()}")
            self.btn2.setStyleSheet(f"background-color: {color.name()}")
            self.btn3.setStyleSheet(f"background-color: {color.name()}")
            self.btn4.setStyleSheet(f"background-color: {color.name()}")
        else:
            print('Turn Off one color')
            self.lb_one_color.setStyleSheet(f"color: #006996")

    @pyqtSlot()
    def tg_br_level_click(self):
        if self.tg_br_level.isChecked():
            print('Br level 2')
        else:
            print('Br level 1')

    @pyqtSlot()
    def tg_on_off_click(self):
        if self.tg_on_off.isChecked():
            self.lb_on_off.setStyleSheet(f"color: #00bcff")
            print('Turn On backlight')
        else:
            self.lb_on_off.setStyleSheet(f"color: #006996")
            print('Turn Off backlight')
        self.bt_submit_click()
    
    @pyqtSlot()
    def tg_wave_direction_click(self):
        if self.tg_wave_direction.isChecked():
            print('Wave to right')
            self.lb_wave_direction.setText('Wave to right >>>')
        else:
            self.lb_wave_direction.setText('Wave to left <<<')
            print('Wave to left')
        
    @pyqtSlot()
    def sl_speed_changed(self):
        print(f'Speed = {self.sl_speed.value()}')
        
    @pyqtSlot()
    def preset_click(self):
        sender = self.sender()
        if sender.isChecked():
            print(sender.text())

    @pyqtSlot()
    def mode_click(self):
        sender = self.sender()
        if sender.isChecked():
            print(sender.text())
            self.effect = sender.text().lower()
            if sender.text() == 'Static':
                # TODO: change slider background color
                self.sl_speed.setEnabled(False)
            else:
                self.sl_speed.setEnabled(True)

    @pyqtSlot()
    def bt_submit_click(self):
        # self.colors = ['ff0000', '00ff00', '0000ff', 'ffffff']
        self.speed = self.sl_speed.value() + 1
        self.brightness = int(self.tg_br_level.isChecked()) + 1
        self.wave_direction = 'ltr' if self.tg_wave_direction.isChecked() else 'rtl'
        print('Submit!!!')
        if self.tg_on_off.isChecked():
            effect = self.effect
        else:
            effect = 'off'
        data = self.led_controller.build_control_string(
            effect=effect,
            colors=self.colors,
            speed=self.speed,
            brightness=self.brightness,
            wave_direction=self.wave_direction
        )
        self.led_controller.send_control_string(data)
        
    def openColorDialog(self):
        color = QColorDialog.getColor()
        if color.isValid():
            print(color.name())
        return color

    def closeEvent(self, event):
        event.accept()
        # event.ignore()
        # self.hide()

if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())
