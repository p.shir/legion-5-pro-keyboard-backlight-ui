from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtProperty

class ModernToggle(QCheckBox):
    """Modern toggle button"""

    def __init__(
        self,
        bg_color = '#004666',
        circle_color = '#DDD',
        active_color = '#00BCFF',
        witdth = 60,
        height = 28,
        animation_curve = QEasingCurve.InOutQuint,
        animation_duraion = 500,
        border_size = 6,
    ):
        QCheckBox.__init__(self)

        self.setFixedSize(witdth, height)

        self._bg_color = bg_color
        self._circle_color = circle_color
        self._active_color = active_color
        self._border_size = border_size

        self._circle_position = 3
        self.animation = QPropertyAnimation(self, b'circle_position', self)        
        self.animation.setEasingCurve(animation_curve)
        self.animation.setDuration(animation_duraion)

        self.stateChanged.connect(self.start_transition)
        
    @pyqtProperty(int)
    def circle_position(self):
        return self._circle_position

    @circle_position.setter
    def circle_position(self, pos):
        self._circle_position = pos
        self.update()

    def start_transition(self, value):
        self.animation.stop()
        if value:
            self.animation.setEndValue(self.width() - self.height() + int(self._border_size / 2))
        else:
            self.animation.setEndValue(int(self._border_size / 2))
        self.animation.start()
        
    def hitButton(self, pos: QPoint):
        return self.contentsRect().contains(pos)
        
    def paintEvent(self, e: QPaintEvent) -> None:
        p = QPainter(self)
        p.setRenderHint(QPainter.Antialiasing)
        
        p.setPen(Qt.NoPen)

        rect = QRect(0, 0, self.width(), self.height())

        w = self.height()- self._border_size
        y = int(self._border_size / 2)
        if not self.isChecked():
            p.setBrush(QColor(self._bg_color))
            p.drawRoundedRect(0, 0, rect.width(), self.height(), self.height() / 2, self.height() / 2)

            p.setBrush(QColor(self._circle_color))
            p.drawEllipse(self._circle_position, y, w, w)
        else:
            p.setBrush(QColor(self._active_color))
            p.drawRoundedRect(0, 0, rect.width(), self.height(), self.height() / 2, self.height() / 2)

            p.setBrush(QColor(self._circle_color))
            p.drawEllipse(self._circle_position, y, w, w)
        
        p.end()
        

class ModernRadioButton(QRadioButton):
    """Modern radio button"""

    def __init__(
        self,
        text = 'Modern RB',
        bg_color = '#004666',
        border_color = '#00BCFF',
        text_active_color = '#00BCFF',
        text_inactive_color = '#006996',
        text_font = None,
        witdth = 100,
        border_size = 10,
        round_part = 6,
    ):
        QRadioButton.__init__(self, text)

        self.setFixedSize(witdth, witdth)

        self._bg_color = bg_color
        self._border_color = border_color
        self._text_active_color = text_active_color
        self._text_inactive_color = text_inactive_color
        self._border_size = border_size
        self._round_part = round_part

        if text_font is not None:
            self._text_font = text_font
        else:
            self._text_font = QFont('Arial', 12)


    def hitButton(self, pos: QPoint):
        return self.contentsRect().contains(pos)
        

    def paintEvent(self, e: QPaintEvent) -> None:
        p = QPainter(self)
        p.setRenderHint(QPainter.Antialiasing)
        
        rect = QRect(0, 0, self.width(), self.height())

        brw = rect.width()  # background rect width
        brh = brw
        frw = rect.width() - self._border_size # foreground rect width
        frh = frw
        frxr = frw / self._round_part  # foreground rect x radius
        fryr = frxr
        frx = int(self._border_size / 2)  # foreground rect x
        fry = frx

        p.setFont(self._text_font)
        pen = p.pen()
        if not self.isChecked():
            p.setBrush(QColor(self._bg_color))
            p.drawRoundedRect(0, 0, brw, brh, frxr, fryr)
            pen.setColor(QColor(self._text_inactive_color))
            p.setPen(pen)
            p.drawText(rect, Qt.AlignCenter, self.text())
        else:
            # big rect (back)
            p.setBrush(QColor(self._border_color))
            p.drawRoundedRect(0, 0, brw, brh, frxr, fryr)
            # small rect (front)
            p.setBrush(QColor(self._bg_color))
            p.drawRoundedRect(frx, fry, frw, frh, frxr, fryr)
            pen.setColor(QColor(self._text_active_color))
            p.setPen(pen)
            p.drawText(rect, Qt.AlignCenter, self.text())
        
        p.end()
        

class ModernSlider(QSlider):
    
    style_template = """
    /* HORIZONTAL */
    QSlider {{ margin: {_margin}px; }}
    QSlider::groove:horizontal {{
        border-radius: {_bg_radius}px;
        height: {_bg_size}px;
        margin: 0px;
        background-color: {_bg_color};
    }}
    QSlider::groove:horizontal:hover {{ background-color: {_bg_color_hover}; }}
    QSlider::handle:horizontal {{
        border: none;
        height: {_handle_size}px;
        width: {_handle_size}px;
        margin: {_handle_margin}px;
        border-radius: {_handle_radius}px;
        background-color: {_handle_color};
    }}
    QSlider::handle:horizontal:hover {{ background-color: {_handle_color_hover}; }}
    QSlider::handle:horizontal:pressed {{ background-color: {_handle_color_pressed}; }}
    /* VERTICAL */
    QSlider::groove:vertical {{
        border-radius: {_bg_radius}px;
        width: {_bg_size}px;
        margin: 0px;
        background-color: {_bg_color};
    }}
    QSlider::groove:vertical:hover {{ background-color: {_bg_color_hover}; }}
    QSlider::handle:vertical {{
        border: none;
        height: {_handle_size}px;
        width: {_handle_size}px;
        margin: {_handle_margin}px;
        border-radius: {_handle_radius}px;
        background-color: {_handle_color};
    }}
    QSlider::handle:vertical:hover {{ background-color: {_handle_color_hover}; }}
    QSlider::handle:vertical:pressed {{ background-color: {_handle_color_pressed}; }}
    """

        # bg_color = '#',
        # border_color = '#00BCFF',
        # text_active_color = '#00BCFF',
    def __init__(
        self,
        margin = 0,
        bg_size = 28,
        bg_radius = 14,
        bg_color = "#004666",
        bg_color_hover = "#005676",
        handle_margin = 2,
        handle_size = 24,
        handle_radius = 12,
        handle_color = "#ddd",
        handle_color_hover = "#ddd",
        handle_color_pressed = "#00bcff"
    ):
        super(ModernSlider, self).__init__()

        adjust_style = self.style_template.format(
            _margin = margin,
            _bg_size = bg_size,
            _bg_radius = bg_radius,
            _bg_color = bg_color,
            _bg_color_hover = bg_color_hover,
            _handle_margin = handle_margin,
            _handle_size = handle_size,
            _handle_radius = handle_radius,
            _handle_color = handle_color,
            _handle_color_hover = handle_color_hover,
            _handle_color_pressed = handle_color_pressed
        )

        self.setStyleSheet(adjust_style)